import torch
import math

class Polynomial3(torch.nn.Module):
    def __init__(self):
        super().__init__()

        self.a = torch.nn.Parameter(torch.randn(()))
        self.b = torch.nn.Parameter(torch.randn(()))
        self.c = torch.nn.Parameter(torch.randn(()))
        self.d = torch.nn.Parameter(torch.randn(()))

    def forward(self, x):
        return self.a + self.b * x + self.c * x ** 2 + self.d * x ** 3

    def string(self):
        return f"y = {self.a.item()} + {self.b.item()} * x + {self.c.item()} * x^2 + {self.d.item()} * x^3"


# input output tensor
x = torch.linspace(-math.pi, math.pi, 2000)
y = torch.sin(x)

# model
model = Polynomial3()

# loss
loss_fn = torch.nn.MSELoss(reduction="sum")
optimizer = torch.optim.SGD(model.parameters(), lr=1e-6)

for t in range(2000):
    # forward
    y_pred = model(x)

    # loss
    loss = loss_fn(y_pred, y)
    if t % 100 == 99:
        print(f"Steps : {t}, loss : {loss.item()}")

    # zero gradients
    optimizer.zero_grad()

    # backward
    loss.backward()

    # update weights
    optimizer.step()

print(f"Result : {model.string()}")

