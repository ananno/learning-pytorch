import torch
import math

# input output
x = torch.linspace(-math.pi, math.pi, 2000)
y = torch.sin(x)

# tensor(x, x^2, x^3)
p = torch.tensor([1, 2, 3])
xx = x.unsqueeze(-1).pow(p)

model = torch.nn.Sequential(
    torch.nn.Linear(3, 1),
    torch.nn.Flatten(0, 1)
)

# loss
loss_fn = torch.nn.MSELoss(reduction='sum')

learning_rate = 1e-6

for t in range(2000):
    # forward
    y_pred = model(xx)

    # loss
    loss = loss_fn(y_pred, y)
    if t % 100 == 99:
        print(f"Steps {t}, loss : {loss}")

    # zero the gradients before backward pass
    model.zero_grad()

    # backward pass
    loss.backward()

    with torch.no_grad():
        for param in model.parameters():
            param -= learning_rate * param.grad
    
linear_layer = model[0]

print(f"Result : y = {linear_layer.bias.item()} + {linear_layer.weight[:, 0].item()} X + {linear_layer.weight[:, 1].item()} x^2 + {linear_layer.weight[:, 2].item()} x^3")