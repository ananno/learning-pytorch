import torch
import math

# input output tensor
x = torch.linspace(-math.pi, math.pi, 2000)
y = torch.sin(x)

# input tensor (x , x^2, x^3)
p = torch.tensor([1, 2, 3])
xx = x.unsqueeze(-1).pow(p)

# model
model = torch.nn.Sequential(
    torch.nn.Linear(3, 1),
    torch.nn.Flatten(0, 1)
)

# loss
loss_fn = torch.nn.MSELoss(reduction='sum')

learning_rate = 1e-3
optimizer = torch.optim.RMSprop(model.parameters(), lr=learning_rate)

for t in range(2000):
    # forward
    y_pred = model(xx)

    # loss
    loss = loss_fn(y_pred, y)
    if t % 100 == 99:
        print(f"Steps {t}, loss : {loss.item()}")

    # zeroing all gradient before backword pass
    optimizer.zero_grad()

    loss.backward()

    # optimize
    optimizer.step()

linear_layer = model[0]
print(f"Result : y = {linear_layer.bias.item()} + {linear_layer.weight[:, 0].item()} x + {linear_layer.weight[:, 1].item()} x^2 + {linear_layer.weight[:, 2].item()} x^3")
